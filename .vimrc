syntax on

set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set relativenumber
set nowrap
set smartcase
set noswapfile
set nobackup
set showcmd
set cursorline
filetype plugin indent on
set wildmenu
set showmatch
set incsearch
set hlsearch

let g:gruvbox_color_column = 'purple'
highlight ColorColumn ctermbg=132
call matchadd('ColorColumn', '\%81v.', 100)

set ruler
set nocompatible
set shortmess+=I
set laststatus=2
set backspace=indent,eol,start
set hidden

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

Plug 'morhetz/gruvbox'
Plug 'tpope/vim-fugitive'
Plug 'Valloric/YouCompleteMe'
Plug 'mbbill/undotree'

call plug#end()

colorscheme gruvbox
set background=dark

